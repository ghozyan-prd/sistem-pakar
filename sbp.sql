-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 26 Mar 2019 pada 14.25
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sbp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `basispengetahuan`
--

CREATE TABLE `basispengetahuan` (
  `namapenyakit` varchar(255) NOT NULL,
  `gejala` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `basispengetahuan`
--

INSERT INTO `basispengetahuan` (`namapenyakit`, `gejala`) VALUES
('batuk', 'demam'),
('pusing', 'tipes'),
('pusing', 'demam'),
('Fish Bites', 'Bekas Luka Terlihat Jelas'),
('Fish Bites', 'Rusaknya Rangka'),
('Fish Bites', 'Bekas Gigitan berwarna Putih'),
('Fish Bites', 'Bekas Luka ditumbuhi algae'),
('Fish Bites', 'Beberapa Luka bisa sembuh'),
('Fish Bites', 'Ditemukan ikan predator di area'),
('CoT Predation', 'Rangka Berwarna putih'),
('CoT Predation', 'Hilangnya Jaringan'),
('CoT Predation', 'Ditemukan CoT di area'),
('CoT Predation', 'Meliputi area yang luas'),
('CoT Predation', 'Pemangsaan di tepi koloni'),
('CoT Predation', 'Bekas pemangasaan di koloni'),
('Drupella Predation', 'Ditemukan drupella di dasar koloni'),
('Drupella Predation', 'Ditemukan siput berwarna putih'),
('Drupella Predation', 'Pemangsaan dari dasar hingga ujung'),
('Drupella Predation', 'Bekas pemangsaan lebih kecil'),
('Drupella Predation', 'Bekas pemangsaan dengan batas tidak teratur'),
('Sedimentation Damage', 'Hilangnya Jaringan'),
('Sedimentation Damage', 'Ditemukan sedimen di permukaan karang'),
('Hama / Penyakit', 'Pertumbuhan algae di jaringan hidup'),
('Black Band Disease', 'Ditemukan cyanobacteria'),
('Black Band Disease', 'Rangka kosong ditumbuhi algae'),
('Black Band Disease', 'Ditemukan band berbentuk annular/linear'),
('Black Band Disease', 'Band menyebar keluar dari luka'),
('Black Band Disease', 'Ditemukan band di karang masif'),
('White Syndromes', 'Laju kehilangan jaringan cepat'),
('White Syndromes', 'Batas luka discerte atau diffuse'),
('White Syndromes', 'Batas luka tidak berpigmen'),
('White Syndromes', 'Luka dekat penyakit berwarna putih'),
('White Syndromes', 'Luka bertahap berubah warna coklat'),
('White Syndromes', 'Garis tepi di luka berbentuk garis'),
('White Syndromes', 'Luka dengan garis tepi annular/linear'),
('White Syndromes', 'Rangka utuh dan terbuka'),
('White Syndromes', 'Tidak ada jaringan antara sehat dan sakit'),
('Yellow Band Disease', 'Laju kehilangan jaringan cepat'),
('Yellow Band Disease', 'Pita paling depan berwarna kuning pucat'),
('Yellow Band Disease', 'Jaringan terinfeksi berwarna lebih gelap'),
('Yellow Band Disease', 'Luka berbentuk focal atau multifocal'),
('Yellow Band Disease', 'Luka dengan garis tepi annular/linear'),
('Yellow Band Disease', 'Luka berbatasan dengan jaringan karang sehat'),
('Yellow Band Disease', 'perkembangan luka dari mm ke cm perbulan'),
('Pigmentation Response', 'Respon terhadap biota pengebor'),
('Pigmentation Response', 'Respon terhadap kompetisi'),
('Pigmentation Response', 'Respon terhadap serangan alga'),
('Pigmentation Response', 'Pola luka berupa multifocal/diffuse'),
('Pigmentation Response', 'Warna jaringan jadi pink/ungu/biru'),
('Pigmentation Response', 'Dinding corallite menebal'),
('Pigmentation Response', 'Pigmentasi berbentuk garis'),
('Pigmentation Response', 'Pigmentasi berbentuk titik'),
('Pigmentation Response', 'Pigmentasi berbentuk benjolan'),
('Pigmentation Response', 'Pigmentasi berbentuk tidak teratur'),
('Trematodiasis', 'Pola luka berupa multifocal/diffuse'),
('Trematodiasis', 'Jaringan membengkak'),
('Trematodiasis', 'jaringan berwarna pink hingga putih'),
('Trematodiasis', 'Ditemukan trematode parasit'),
('Bleaching', 'Karang masih hidup'),
('Bleaching', 'Adanya gradien jaringan sehat dengan sakit'),
('Bleaching', 'Hilangnya alga simbion pada karang'),
('Bleaching', 'Tekanan terhadap lingkungan (suhu)'),
('Bleaching', 'Polip masih terlihat'),
('Unusual Bleaching Pattern', 'Area jaringan berwarna putih'),
('Unusual Bleaching Pattern', 'Jaringan masih ada'),
('Unusual Bleaching Pattern', 'Ditemukan garis dengan batas yang jelas'),
('Unusual Bleaching Pattern', 'Tingkat pemutihan karang bervariasi'),
('Unusual Bleaching Pattern', 'Hilangnya alga simbion pada karang'),
('Unusual Bleaching Pattern', 'Tidak ada pigmentasi jaringan'),
('Galls', 'Deformasi rangka berbentuk focal/multifocal'),
('Galls', 'Ditemukan invertebrata'),
('Galls', 'Deformasi rangka tidak beraturan'),
('Galls', 'Coenesteum menebal'),
('Growth Anomalies of Unknown Cause', 'Berwarna lebih muda'),
('Growth Anomalies of Unknown Cause', 'Tidak berwarna sama sekali'),
('Growth Anomalies of Unknown Cause', 'Luka berbentuk circular hingga difuse');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gejala`
--

CREATE TABLE `gejala` (
  `idgejala` varchar(255) NOT NULL,
  `gejala` varchar(255) NOT NULL,
  `daerah` varchar(255) NOT NULL,
  `jenistanaman` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gejala`
--

INSERT INTO `gejala` (`idgejala`, `gejala`, `daerah`, `jenistanaman`) VALUES
('G001', 'Bekas Luka Terlihat Jelas', 'Rangka', 'Terumbu Karang'),
('G002', 'Rusaknya Rangka', 'Rangka', 'Terumbu Karang'),
('G003', 'Bekas Gigitan berwarna Putih', 'Rangka', 'Terumbu Karang'),
('G004', 'Bekas Luka ditumbuhi algae', 'Rangka', 'Terumbu Karang'),
('G005', 'Beberapa Luka bisa sembuh', 'Rangka', 'Terumbu Karang'),
('G006', 'Ditemukan ikan predator di area', 'Rangka', 'Terumbu Karang'),
('G007', 'Hilangnya Jaringan', 'jaringan', 'Terumbu Karang'),
('G008', 'Rangka Berwarna putih', 'Rangka', 'Terumbu Karang'),
('G009', 'Ditemukan CoT di area', 'jaringan', 'Terumbu Karang'),
('G010', 'Meliputi area yang luas', 'jaringan', 'Terumbu Karang'),
('G011', 'Pemangsaan di tepi koloni', 'Koloni', 'Terumbu Karang'),
('G012', 'Bekas pemangasaan di koloni', 'Koloni', 'Terumbu Karang'),
('G013', 'Ditemukan siput berwarna putih', 'Cabang Karang', 'Terumbu Karang'),
('G014', 'Pemangsaan dari dasar hingga ujung', 'Cabang Karang', 'Terumbu Karang'),
('G015', 'Bekas pemangsaan lebih kecil', 'Cabang Karang', 'Terumbu Karang'),
('G016', 'Bekas pemangsaan dengan batas tidak teratur', 'Cabang Karang', 'Terumbu Karang'),
('G017', 'Ditemukan drupella di dasar koloni', 'Koloni', 'Terumbu Karang'),
('G018', 'Ditemukan sedimen di permukaan karang', 'Permukaan', 'Terumbu Karang'),
('G019', 'Pertumbuhan algae di jaringan hidup', 'jaringan', 'Terumbu Karang'),
('G020', 'Ditemukan band berbentuk annular/linear', 'jaringan', 'Terumbu Karang'),
('G021', 'Ditemukan band berwarna hitam/coklat', 'jaringan', 'Terumbu Karang'),
('G022', 'Ditemukan cyanobacteria', 'Rangka', 'Terumbu Karang'),
('G023', 'Band menyebar keluar dari luka', 'Luka', 'Terumbu Karang'),
('G024', 'Ditemukan band di karang masif', 'Luka', 'Terumbu Karang'),
('G025', 'Rangka kosong ditumbuhi algae', 'Rangka', 'Terumbu Karang'),
('G026', 'Batas luka discerte atau diffuse', 'Luka', 'Terumbu Karang'),
('G027', 'Batas luka tidak berpigmen', 'Luka', 'Terumbu Karang'),
('G028', 'Laju kehilangan jaringan cepat', 'jaringan', 'Terumbu Karang'),
('G029', 'Luka dekat penyakit berwarna putih', 'Luka', 'Terumbu Karang'),
('G030', 'Luka bertahap berubah warna coklat', 'Luka', 'Terumbu Karang'),
('G031', 'Garis tepi di luka berbentuk garis', 'Luka', 'Terumbu Karang'),
('G032', 'Luka berbentuk focal atau multifocal', 'Luka', 'Terumbu Karang'),
('G033', 'Luka dengan garis tepi annular/linear', 'Luka', 'Terumbu Karang'),
('G034', 'Luka berbatasan dengan jaringan karang sehat', 'Luka', 'Terumbu Karang'),
('G035', 'perkembangan luka dari mm ke cm perbulan', 'Luka', 'Terumbu Karang'),
('G036', 'Pita paling depan berwarna kuning pucat', 'jaringan', 'Terumbu Karang'),
('G037', 'Jaringan terinfeksi berwarna lebih gelap', 'jaringan', 'Terumbu Karang'),
('G038', 'Pola luka berupa multifocal/diffuse', 'jaringan', 'Terumbu Karang'),
('G039', 'Warna jaringan jadi pink/ungu/biru', 'jaringan', 'Terumbu Karang'),
('G040', 'Dinding corallite menebal', 'jaringan', 'Terumbu Karang'),
('G041', 'Pigmentasi berbentuk garis', 'jaringan', 'Terumbu Karang'),
('G042', 'Pigmentasi berbentuk titik', 'jaringan', 'Terumbu Karang'),
('G043', 'Pigmentasi berbentuk benjolan', 'jaringan', 'Terumbu Karang'),
('G044', 'Pigmentasi berbentuk tidak teratur', 'jaringan', 'Terumbu Karang'),
('G045', 'Respon terhadap biota pengebor', 'Rangka', 'Terumbu Karang'),
('G046', 'Respon terhadap kompetisi', 'Rangka', 'Terumbu Karang'),
('G047', 'Respon terhadap serangan alga', 'Rangka', 'Terumbu Karang'),
('G048', 'Jaringan membengkak', 'jaringan', 'Terumbu Karang'),
('G049', 'jaringan berwarna pink hingga putih', 'jaringan', 'Terumbu Karang'),
('G050', 'Ditemukan trematode parasit', 'jaringan', 'Terumbu Karang'),
('G051', 'Karang masih hidup', 'Rangka', 'Terumbu Karang'),
('G052', 'Polip masih terlihat', 'Polip', 'Terumbu Karang'),
('G053', 'Adanya gradien jaringan sehat dengan sakit', 'jaringan', 'Terumbu Karang'),
('G054', 'Hilangnya alga simbion pada karang', 'Zooxanthellae', 'Terumbu Karang'),
('G056', 'Area jaringan berwarna putih', 'jaringan', 'Terumbu Karang'),
('G057', 'Jaringan masih ada', 'jaringan', 'Terumbu Karang'),
('G058', 'Tidak ada pigmentasi jaringan', 'jaringan', 'Terumbu Karang'),
('G059', 'Ditemukan garis dengan batas yang jelas', 'jaringan', 'Terumbu Karang'),
('G060', 'Tingkat pemutihan karang bervariasi', 'jaringan', 'Terumbu Karang'),
('G061', 'Deformasi rangka berbentuk focal/multifocal', 'Rangka', 'Terumbu Karang'),
('G062', 'Ditemukan invertebrata', 'Rangka', 'Terumbu Karang'),
('G063', 'Deformasi rangka tidak beraturan', 'Rangka', 'Terumbu Karang'),
('G064', 'Coenesteum menebal', 'Rangka', 'Terumbu Karang'),
('G065', 'Luka berbentuk circular hingga difuse', 'Luka', 'Terumbu Karang'),
('G066', 'Berwarna lebih muda', 'jaringan', 'Terumbu Karang'),
('G067', 'Tidak berwarna sama sekali', 'jaringan', 'Terumbu Karang'),
('G068', 'Tidak ada jaringan antara sehat dan sakit', 'jaringan', 'Terumbu Karang'),
('G069', 'Rangka utuh dan terbuka', 'Rangka', 'Terumbu Karang'),
('H055', 'Tekanan terhadap lingkungan (suhu)', 'Zooxanthellae', 'Terumbu Karang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyakit`
--

CREATE TABLE `penyakit` (
  `idpenyakit` varchar(20) NOT NULL,
  `namapenyakit` varchar(1000) NOT NULL,
  `jenistanaman` varchar(255) NOT NULL,
  `kulturteknis` varchar(1000) NOT NULL,
  `fisikmekanis` varchar(1000) NOT NULL,
  `kimiawi` varchar(1000) NOT NULL,
  `hayati` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penyakit`
--

INSERT INTO `penyakit` (`idpenyakit`, `namapenyakit`, `jenistanaman`, `kulturteknis`, `fisikmekanis`, `kimiawi`, `hayati`) VALUES
('P001', 'Fish Bites', 'Terumbu Karang', '', '', '', ''),
('P002', 'CoT Predation', 'Terumbu Karang', '', '', '', ''),
('P003', 'Drupella Predation', 'Terumbu Karang', '', '', '', ''),
('P004', 'Sedimentation Damage', 'Terumbu Karang', '', '', '', ''),
('P005', 'Algae Overgrowth', 'Terumbu Karang', '', '', '', ''),
('P006', 'Black Band Disease', 'Terumbu Karang', '', '', '', ''),
('P007', 'White Syndromes', 'Terumbu Karang', '', '', '', ''),
('P008', 'Yellow Band Disease', 'Terumbu Karang', '', '', '', ''),
('P009', 'Pigmentation Response', 'Terumbu Karang', '', '', '', ''),
('P010', 'Trematodiasis', 'Terumbu Karang', '', '', '', ''),
('P011', 'Bleaching', 'Terumbu Karang', '', '', '', ''),
('P012', 'Unusual Bleaching Pattern', 'Terumbu Karang', '', '', '', ''),
('P013', 'Galls', 'Terumbu Karang', '', '', '', ''),
('P014', 'Growth Anomalies of Unknown Cause', 'Terumbu Karang', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`iduser`, `username`, `password`, `nama`) VALUES
('U001', 'admin', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`idgejala`);

--
-- Indeks untuk tabel `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`idpenyakit`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
