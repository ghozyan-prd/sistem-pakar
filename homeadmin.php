<?php
include "session.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sistem Pakar</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      </ul>
      <ul class="nav navbar-nav navbar-right">
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid text-center">
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <p><a href="homeadmin.php"><button type="button" class="btn btn-primary btn-block active">BERANDA</button></a></p>
      <p><a href="hamadanpenyakit.php"><button type="button" class="btn btn-primary btn-block">HAMA dan PENYAKIT</button></a></p>
      <p><a href="gejala.php"><button type="button" class="btn btn-primary btn-block">GEJALA</button></a></p>
        <p><a href="basispengetahuan.php"><button type="button" class="btn btn-primary btn-block">BASIS PENGETAHUAN</button></a></p>
      <br><br><br><br><br><br><br><br><br><br>
      <p><a href="logout.php"><button type="button" class="btn btn-primary btn-block" id="myBtn">LOGOUT</button></a></p>
    </div>
    <div class="col-sm-8 text-left">
       <center><h2>SISTEM PAKAR DIAGNOSA PENYAKIT PADA TERUMBU KARANG
</h2></center><br>
      <p>Seiring perkembangan teknologi, dikembangkan sebuah sistem yang mampu mengadopsi proses dan cara berpikir manusia yaitu sistem pakar yang mengandung pengetahuan tertentu sehingga setiap orang dapat menggunakannya untuk memecahkan masalah bersifat spesifik yaitu permasalahan diagnosis penyakit pada terumbu karang. Tujuan dari penelitian ini adalah membangun sistem pakar untuk mendiagnosa penyakit pada terumbu karang dalam bentuk website menggunakan pemrograman PHP dengan database MySQL. Sistem pakar untuk mendiagnosa penyakit pada terumbu karang menggunakan metode Ripple Down Rules (RDR) ini bertujuan menelusuri gejala yang ditampilkan dalam bentuk pertanyaan – pertanyaan agar dapat mendiagnosa jenis penyakit dengan berbasis website. Sistem pakar berbasis web mampu mengenali jenis penyakit pada terumbu karang setelah melakukan konsultasi dengan menjawab beberapa pertanyaan – pertanyaan yang ditampilkan oleh aplikasi sistem pakar serta dapat menyimpulkan beberapa jenis penyakit pada terumbu karang. Data penyakit yang dikenali menyesuaikan rules (aturan) yang dibuat untuk dapat mencocokkan gejala-gejala penyakit pada terumbu karang.
</p>
    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <p>S1-Sistem Informasi 2016 UINSA</p>
</footer>

</body>
</html>
